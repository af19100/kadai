import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;
public class GUI {

    int total = 0;
    String a="";

    void order(String food,int kakaku){

        JTextArea textArea1 = new JTextArea("");

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Comfirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be serves as soon as possibile!");

            total+=kakaku;
            a+=food+"\n";

            textBOX1.setText(a);

            textField1.setText("total"+total+"yen");
        }
        if(confirmation == 1){
        }
    }

    private JPanel root;
    private JButton tempuraButton;
    private JButton karageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane textBOX1;
    private JButton checkOutButton;
    private JTextField textField1;
    private JTextPane textBOX2;

    public GUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 100);
            }
        });
        karageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karage", 110);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 120);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 130);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 140);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 150);
            }
        });
        tempuraButton.setIcon(new ImageIcon
                (this.getClass().getResource("1.png")));
        karageButton.setIcon(new ImageIcon
                (this.getClass().getResource("2.png")));
        gyozaButton.setIcon(new ImageIcon
                (this.getClass().getResource("3.png")));
        udonButton.setIcon(new ImageIcon
                (this.getClass().getResource("4.png")));
        yakisobaButton.setIcon(new ImageIcon
                (this.getClass().getResource("5.png")));
        ramenButton.setIcon(new ImageIcon
                (this.getClass().getResource("6.png")));


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Comfirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you.The total price is " + total + " yen.");
                    a="";
                    textBOX1.setText(a);
                    total=0;
                    textField1.setText("total" + total + "yen");
                }
                if(confirmation == 1){
                }
            }

        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI");
        frame.setContentPane(new GUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
